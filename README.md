# Pebble Restaurent Waiting List Server

## To create pipenv
N.B. Only tested on Ubuntu based system

`pip3 install pipenv`

`pipenv install --dev`

## To setup DB
`pipenv run python manage.py migrate`

## To run server
`pipenv run python manage.py runserver`

## To run Django unit tests
`pipenv run python manage.py test`