from django.urls import include, path
from rest_framework import routers
from pebble.app.views import RestaurantViewSet, WaitingListViewSet

router = routers.DefaultRouter()
router.register(r'restaurants', RestaurantViewSet)
router.register(r'waitlists', WaitingListViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls))
]

# POST /restaurants
# GET /restaurants
# POST /waitlist
# DELETE /waitlist/{id}
# GET /restaurants/{id}/waitlist
