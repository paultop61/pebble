from django.db import models


class Restaurant(models.Model):
    class Meta:
        app_label = 'pebble'

    name = models.CharField(max_length=30, unique=True)


class WaitListEntry(models.Model):
    class Meta:
        app_label = 'pebble'

    name = models.CharField(max_length=30)
    email = models.EmailField(max_length=254)
    timestamp = models.DateTimeField(auto_now_add=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
