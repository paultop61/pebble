from django.contrib import admin
from app.models import Restaurant, RestaurantWaitList

# Register your models here.

admin.site.register(Restaurant)
admin.site.register(RestaurantWaitList)
