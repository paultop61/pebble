from django.test import TestCase
from pebble.app.models import Restaurant, WaitListEntry
from django.db.utils import IntegrityError

# Create your tests here.


class RestaurantTestCase(TestCase):
    def test_restaurant_get(self):
        Restaurant.objects.create(name="Test Restaurant")
        restaurant = Restaurant.objects.get(name="Test Restaurant")

        self.assertEqual(restaurant.name, "Test Restaurant")

    def test_restaurant_create(self):
        self.assertIsNotNone(
            Restaurant.objects.create(
                name="Test Restaurant 2"
            )
        )

    def test_restaurant_update(self):
        Restaurant.objects.create(name="Test Restaurant 3")
        restaurant = Restaurant.objects.get(name="Test Restaurant 3")
        restaurant.name = "Test Restaurant 3 Updated"
        restaurant.save()
        self.assertEqual(restaurant.name, "Test Restaurant 3 Updated")

    def test_restaurant_delete(self):
        Restaurant.objects.create(name="Test Restaurant 4")
        restaurant = Restaurant.objects.get(name="Test Restaurant 4")
        restaurant.delete()
        with self.assertRaisesMessage(
            Restaurant.DoesNotExist,
            "Restaurant matching query does not exist."
        ):
            Restaurant.objects.get(name="Test Restaurant 4")


class WaitingListTestCase(TestCase):
    def test_waiting_list_entry_get(self):
        Restaurant.objects.create(id=1, name="Test Restaurant")
        Restaurant.objects.create(id=2, name="Test Restaurant 2")
        WaitListEntry.objects.create(
            name="Test User", email="test_user@email.com", restaurant_id=1
        )
        WaitListEntry.objects.create(
            name="Test User 2", email="test_user@email.com", restaurant_id=2
        )
        entry = WaitListEntry.objects.get(name="Test User")

        self.assertEqual(entry.name, "Test User")

        entries = WaitListEntry.objects.filter(restaurant_id=2)

        self.assertEquals(1, len(entries))

    def test_waiting_list_entry_create(self):
        Restaurant.objects.create(id=2, name="Test Restaurant 2")
        self.assertIsNotNone(
            WaitListEntry.objects.create(
                name="Test User", email="test_user@email.com", restaurant_id=2
            )
        )

        with self.assertRaisesMessage(
            IntegrityError,
            "NOT NULL constraint failed: pebble_waitlistentry.restaurant_id"
        ):
            WaitListEntry.objects.create(
                name="Test User", email="test_user@email.com"
            )

    def test_waiting_list_entry_update(self):
        Restaurant.objects.create(id=3, name="Test Restaurant 3")
        WaitListEntry.objects.create(
            name="Test User 3", email="test_user@email.com", restaurant_id=3
        )
        entry = WaitListEntry.objects.get(name="Test User 3")

        entry.name = "Test User 3 Update"
        entry.save()

        self.assertEqual(entry.name, "Test User 3 Update")

    def test_waiting_list_entry_delete(self):
        Restaurant.objects.create(id=4, name="Test Restaurant 4")
        WaitListEntry.objects.create(
            name="Test User 4", email="test_user@email.com", restaurant_id=4
        )
        entry = WaitListEntry.objects.get(name="Test User 4")

        entry.delete()
        with self.assertRaisesMessage(
            WaitListEntry.DoesNotExist,
            "WaitListEntry matching query does not exist."
        ):
            WaitListEntry.objects.get(name="Test User 4")
