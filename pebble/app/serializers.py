from rest_framework import serializers
from pebble.app.models import Restaurant, WaitListEntry


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = "__all__"


class WaitListEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = WaitListEntry
        fields = "__all__"
