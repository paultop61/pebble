from rest_framework import viewsets
from pebble.app.serializers import (
    RestaurantSerializer, WaitListEntrySerializer
)
from pebble.app.models import Restaurant, WaitListEntry


class RestaurantViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows restaurants to be viewed or edited.
    """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer


class WaitingListViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows restaurant waitlists to be viewed or edited.
    """
    queryset = WaitListEntry.objects.all().order_by('-timestamp')
    serializer_class = WaitListEntrySerializer

    def get_queryset(self):
        restaurant_id = self.request.query_params.get('restaurant_id')

        queryset = WaitListEntry.objects.filter(
            restaurant__id=restaurant_id
        ).order_by('-timestamp')

        return queryset
