from django.apps import AppConfig


class PebbleAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pebble'
